// Compiled using marko@4.14.17 - DO NOT EDIT
"use strict";

var marko_template = module.exports = require("marko/src/html").t(__filename),
    marko_componentType = "/simple$1.0.0/src/components/login/index.marko",
    components_helpers = require("marko/src/components/helpers"),
    marko_renderer = components_helpers.r,
    marko_defineComponent = components_helpers.c;

function render(input, out, __component, component, state) {
  var data = input;

  out.w("<div class=\"wrapper\"><div class=\"container\"><h1>Welcome</h1><form class=\"form\"><input type=\"text\" placeholder=\"Email\"><input type=\"password\" placeholder=\"Password\"><button type=\"submit\" id=\"login-button\">Login</button><div class=\"progress\"><div class=\"indeterminate\"></div></div></form></div><ul class=\"bg-bubbles\"><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li></ul></div> ");
}

marko_template._ = marko_renderer(render, {
    ___implicit: true,
    ___type: marko_componentType
  });

marko_template.Component = marko_defineComponent({}, marko_template._);

marko_template.meta = {
    deps: [
      "./style.css"
    ],
    id: "/simple$1.0.0/src/components/login/index.marko"
  };
